Ansible module used by OSAS to manage munin.

This module requires the httpd module of OSAS to work, 
it can be found on https://github.com/OSAS/ansible-role-httpd
or https://gitlab.com/osas/ansible-role-httpd

# Example of playbook

This role deploy both the server and the agents needed for munin, 
using the variable `munin_master` to decide where the master will be running.

So to use it, the proposed playbook would be similar to this one:
```
$ cat deploy_munin.yml
- hosts: all
  roles:
  - role: munin
    munin_master: munin.example.org
```

The role take care of opening the firewall on Redhat-like system, but for EL6, the
port is opened to all internet (mostly due to lazyness, and lack of good interface for that
in lokki).

# Alerting

Munin can also send alert based on threshold. While a dedicated system is usually
more useful for that, this can be enough for small enough setup.

To use it, you can just set the variable `admin_email` to the email that should be
notified. 

For example:

```
$ cat deploy_munin.yml
- hosts: all
  roles:
  - role: munin
    munin_master: munin.example.org
    admin_email: root@example.org
```

# Advanced options

Since this role inherit from OSAS httpd role, it can also 
reuse the same options, since variables are automatically pushed
to dependents roles.

For example, adding TLS support with let's encrypt can be 
done like this:

```
$ cat deploy_munin.yml
- hosts: all
  roles:
  - role: munin
    munin_master: munin.example.org
    use_letsencrypt: True
```

# Dealing with nat firewalling

The role automatically detect the ip used on the munin master to
connect to the node. However, this assume that the master is directly
connected on the internet with a routable ip, which may not be the case.

If that happen, the autodetected ip can be overriden with the variable
`munin_master_ip`.
